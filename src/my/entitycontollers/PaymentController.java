package my.entitycontollers;

import java.util.Date;
import java.util.List;

import my.entities.Payment;
import my.entities.Payment;
import my.entities.Payment;
import my.utils.Util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class PaymentController {
	@SuppressWarnings("unchecked")
	public List<Payment> getAllPayments()
	{
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		List<Payment> users = null;
		try
		{
			users = (List<Payment>)s.createQuery("from Payment").list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
		return users;
	}
	public void editPayment(Payment u)
	{
		Payment p = null;
		List<Payment> currentPayments = getAllPayments();
		for(Payment u2 : currentPayments)
			if(u.getId() == u2.getId())
			{
				p = u2;
				break;
			}
		if(p == null)
			return;
		p.setAmount(u.getAmount());
		p.setDate(new Date(0));
		p.setOrderid(u.getOrderid());
		p.setUserid(u.getUserid());
		p.setUsername(u.getUsername());
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.update(p);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
	
	public void addPayment(Payment u)
	{
		//first, check if user (by name) is already in DB
		//
		List<Payment> currentPayments = getAllPayments();
		for(Payment u2 : currentPayments)
			if(u.getId() == u2.getId())
			{
				System.out.println("Payment Already Exists...");
				return;
			}
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.save(u);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
	public Payment getPayment(Payment p)
	{
		List<Payment> currentPayment = getAllPayments();
		for(Payment u : currentPayment)
			if(u.getId() == p.getId())
					return u;
		return null;
	}
}
