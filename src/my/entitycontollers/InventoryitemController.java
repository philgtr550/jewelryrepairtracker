package my.entitycontollers;

import java.util.List;

import my.entities.Inventoryitem;
import my.entities.Inventoryitem;
import my.utils.Util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class InventoryitemController {
	@SuppressWarnings("unchecked")
	public List<Inventoryitem> getAllInventoryitems()
	{
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		List<Inventoryitem> inventoryitems = null;
		try
		{
			inventoryitems = (List<Inventoryitem>)s.createQuery("from Inventoryitem").list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
		return inventoryitems;
	}
	
	public Inventoryitem getInventoryitem(Inventoryitem i)
	{
		List<Inventoryitem> currentInventoryitems = getAllInventoryitems();
		for(Inventoryitem u : currentInventoryitems)
			if(i.getId() == u.getId())
					return u;
		return null;
	}
	
	public void editInventoryitem(Inventoryitem u)
	{
		Inventoryitem item = null;
		List<Inventoryitem> currentInventoryitems = getAllInventoryitems();
		for(Inventoryitem u2 : currentInventoryitems)
			if(u.getId() == u2.getId())
			{
				item = u2;
				break;
			}
		if(item == null)
			return;
		item.setDescr("Plamps the goit");
		item.setItemname("AAAAAAAA yourself!");
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.update(item);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
	
	public void addInventoryitem(Inventoryitem u)
	{
		//first, check if user (by name) is already in DB
		//
		List<Inventoryitem> currentInventoryitems = getAllInventoryitems();
		for(Inventoryitem u2 : currentInventoryitems)
			if(u.getId() == u2.getId())
			{
				System.out.println("Inventoryitem Already Exists...");
				return;
			}
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.save(u);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
}
