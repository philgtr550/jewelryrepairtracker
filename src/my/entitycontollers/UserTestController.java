package my.entitycontollers;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.sql.Date;
import java.util.List;

import my.entities.*;
import my.utils.Util;
public class UserTestController {

	public static void main(String[] args) 
	{	
		Payment p = new Payment();
		PaymentController pc = new PaymentController();
		p.setAmount(90.99f);
		p.setOrderid(0);
		p.setUserid(7);
		p.setUsername("goit");
		p.setDate(new Date(9,9,9));
		pc.getAllPayments();
		pc.addPayment(p);
		InventoryitemController ic = new InventoryitemController();
		ic.getAllInventoryitems();
		Inventoryitem i = new Inventoryitem();
		i.setDescr("Plamps the goit");
		i.setItemname("AAAAAAAA yourself!");
		i.setQty(1000);
		ic.addInventoryitem(i);
		ServiceController sc = new ServiceController();
		Service ss = new Service();
		ss.setName("Plamp the goit");
		ss.setType(0);
		sc.addService(ss);
		OrderController oc = new OrderController();
		oc.getOrders();
		System.exit(0);
	}

}
