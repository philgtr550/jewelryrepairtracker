package my.entitycontollers;

import java.sql.Date;
import java.util.List;

import my.entities.Order;
import my.utils.Util;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class OrderController 
{
	@SuppressWarnings("unchecked")
	public List<Order> getOrders()
	{
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		List<Order> orders = null;
		try
		{
			Query q = s.createQuery("from Order");
			orders = q.list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
		return orders;
	}

	public Order getOrder(int id)
	{
		List<Order> currentOrders = getOrders();
		for(Order u2 : currentOrders)
			if(id == u2.getId())
					return u2;
		return null;
	}
	
	public Order getOrder(Order u)
	{
		List<Order> currentOrders = getOrders();
		for(Order u2 : currentOrders)
			if(u.getId() == u2.getId())
					return u;
		return null;
	}
	
	public void editOrder(Order u)
	{
		Order o = null;
		List<Order> currentOrders = getOrders();
		for(Order u2 : currentOrders)
			if(u.getId() == u2.getId())
			{
				o = u2;
				break;
			}
		if(o == null)
			return;
		
		o.setFinishdate(u.getFinishdate());
		o.setPaid(u.isPaid());
		o.setInventoryused(1);
		o.setPriority(u.getPriority());
		o.setReqservice(u.getReqservice());
		o.setStatus(u.getStatus());
		o.setUsername(u.getUsername());
		o.setUserid(u.getUserid());
		o.setType(u.getType());
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.update(o);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
	
	public void addOrder(Order u)
	{
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.save(u);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
}
