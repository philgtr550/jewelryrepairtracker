package my.entitycontollers;

import java.util.List;

import my.entities.Service;
import my.utils.Util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ServiceController {
	@SuppressWarnings("unchecked")
	public List<Service> getAllServices()
	{
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		List<Service> inventoryitems = null;
		try
		{
			inventoryitems = (List<Service>)s.createQuery("from Service").list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
		return inventoryitems;
	}
	
	public Service getService(Service i)
	{
		List<Service> currentServices = getAllServices();
		for(Service u : currentServices)
			if(i.getId() == u.getId())
					return u;
		return null;
	}
	
	public void editService(Service u)
	{
		Service serv = null;
		List<Service> currentServices = getAllServices();
		for(Service u2 : currentServices)
			if(u.getId() == u2.getId())
			{
				serv = u2;
				break;
			}
		if(serv == null)
			return;
		serv.setName(u.getName());
		serv.setType(u.getType());
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.update(serv);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
	
	public void addService(Service u)
	{
		//first, check if user (by name) is already in DB
		//
		List<Service> currentServices = getAllServices();
		for(Service u2 : currentServices)
			if(u.getId() == u2.getId())
			{
				System.out.println("Service Already Exists...");
				return;
			}
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.save(u);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
}
