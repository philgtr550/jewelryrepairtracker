package my.entitycontollers;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import my.entities.User;
import my.utils.Util;

public class UserController 
{
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers()
	{
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		List<User> users = null;
		try
		{
			users = (List<User>)s.createQuery("from User").list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
		return users;
	}
	
	public User getUser(String name, String pass)
	{
		List<User> currentUsers = getAllUsers();
		for(User u : currentUsers)
			if(name.equals(u.getUsername()) && pass.equals(u.getPass()))
					return u;
		return null;
	}
	
	public User getUser(String name)
	{
		List<User> currentUsers = getAllUsers();
		for(User u : currentUsers)
			if(name.equals(u.getUsername()))
					return u;
		return null;
	}
	public User getUser(User u)
	{
		List<User> currentUsers = getAllUsers();
		for(User u2 : currentUsers)
			if(u.getId() == u2.getId())
					return u;
		return null;
	}
	
	public void editUser(User u)
	{
		User internalUser = null;
		List<User> currentUsers = getAllUsers();
		for(User u2 : currentUsers)
			if(u.getId() == u2.getId())
			{
				internalUser = u2;
				break;
			}
		if(internalUser == null)
			return;
		internalUser.setAdmin(u.isAdmin());
		internalUser.setComments(u.getComments());
		internalUser.setEmail(u.getEmail());
		internalUser.setPass(u.getPass());
		internalUser.setPhone(u.getPhone());
		internalUser.setUsername(u.getUsername());
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.update(internalUser);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
	
	public void addUser(User u)
	{
		//first, check if user (by name) is already in DB
		//
		List<User> currentUsers = getAllUsers();
		for(User u2 : currentUsers)
			if(u.getUsername().equalsIgnoreCase(u2.getUsername()))
			{
				System.out.println("User Already Exists...");
				return;
			}
		SessionFactory sf = Util.getSessionFactory();
		Session s = sf.openSession();
		s.beginTransaction();
		try
		{
			s.save(u);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			s.getTransaction().rollback();
		}
		s.getTransaction().commit();
		s.close();
	}
}
