package my.exec;

import java.util.List;

import javax.swing.JFrame;

import my.entities.User;
import my.entitycontollers.UserController;
import my.screens.AdminCreateAdmin;
import my.screens.LoginScreen;

public class RunMe {

	public static void main(String[] args) 
	{
		UserController uc = new UserController();
		List<User> users = uc.getAllUsers();
		JFrame start = null;
		if(users.size() == 0)
		{
			start = new AdminCreateAdmin(true);
			start.setVisible(true);
		}
		else
		{
			start = new LoginScreen();
			start.setVisible(true);
		}
	}

}
