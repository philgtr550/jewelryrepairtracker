package my.entities;
import javax.persistence.*;
// default package
// Generated Jun 22, 2014 8:56:45 PM by Hibernate Tools 4.0.0

/**
 * InventoryitemId generated by hbm2java
 */
@Entity
@Table(name = "inventoryitem")
public class Inventoryitem implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400922699712092902L;
	private int id;
	private String itemname;
	private int qty;
	private String descr;

	public Inventoryitem() {
	}

	public Inventoryitem(int id, String itemname, int qty, String descr) {
		this.id = id;
		this.itemname = itemname;
		this.qty = qty;
		this.descr = descr;
	}

	@Id
	@GeneratedValue
	@Column(name = "id")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "itemname")
	public String getItemname() {
		return this.itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	@Column(name = "qty")
	public int getQty() {
		return this.qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
	@Column(name = "descr")
	public String getDescr() {
		return this.descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof Inventoryitem))
			return false;
		Inventoryitem castOther = (Inventoryitem) other;

		return (this.getId() == castOther.getId())
				&& ((this.getItemname() == castOther.getItemname()) || (this
						.getItemname() != null
						&& castOther.getItemname() != null && this
						.getItemname().equals(castOther.getItemname())))
				&& (this.getQty() == castOther.getQty())
				&& ((this.getDescr() == castOther.getDescr()) || (this
						.getDescr() != null && castOther.getDescr() != null && this
						.getDescr().equals(castOther.getDescr())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getId();
		result = 37 * result
				+ (getItemname() == null ? 0 : this.getItemname().hashCode());
		result = 37 * result + this.getQty();
		result = 37 * result
				+ (getDescr() == null ? 0 : this.getDescr().hashCode());
		return result;
	}

}
