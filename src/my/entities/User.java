package my.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

// default package
// Generated Jun 22, 2014 8:56:45 PM by Hibernate Tools 4.0.0


@Entity
@Table(name = "user")
public class User implements java.io.Serializable {

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("User ID: " + id + " | Username: " + username + " | Comments: " + comments);
		return sb.toString();
	//return "User [id=" + id + ", phone=" + phone + ", email=" + email
	//			+ ", comments=" + comments + ", admin=" + admin + ", username="
		//		+ username + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5096477373753872810L;
	private int id;
	private int phone;	//obtained from text field
	private String pass;
	private String email;//obtained from text field
	private String comments; //obtained from text field
	private boolean admin; //default: false
	private String username; 

	public User() {	//bomb
		throw new UnsupportedOperationException("Default constructor prohibited");
	}

	public User(int id, int phone, String pass, String email,
			String comments, boolean admin, String username) {
		this.id = id;
		this.phone = phone;
		this.pass = pass;
		this.email = email;
		this.comments = comments;
		this.admin = admin;
		this.username = username;
	}

	@Id
	@GeneratedValue
	@Column(name = "id")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "phone")
	public int getPhone() {
		return this.phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	@Column(name = "pass")
	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@Column(name = "email")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "comments")
	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Column(name = "admin")
	public boolean isAdmin() {
		return this.admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Column(name = "username")
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
