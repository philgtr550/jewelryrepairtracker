package my.utils;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;;

public class Util 
{
	private static SessionFactory sf;
	private static ServiceRegistry sr;
	
	@SuppressWarnings("deprecation")
	private static SessionFactory configSF() throws HibernateException
	{
		Configuration c = new Configuration();
		c.configure();
		sr = new ServiceRegistryBuilder().applySettings(c.getProperties()).build();
		sf = c.buildSessionFactory(sr);
		return sf;
	}
	
	public static SessionFactory getSessionFactory()
	{
		return configSF();
	}
}
